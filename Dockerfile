# Utilizamos una imagen de node como base
FROM node:latest

# Establecemos el directorio de trabajo en el contenedor
WORKDIR /app

# Copiamos los archivos de la aplicación al directorio de trabajo
COPY package.json package-lock.json ./

# Instalamos las dependencias del proyecto
RUN npm install

# Copiamos el resto de los archivos de la aplicación al directorio de trabajo
COPY . .

# Comando para iniciar la aplicación cuando se ejecute el contenedor
CMD ["node", "main.js"]


# docker build -t backforfrontend_imagen .
# docker stop backforfrontend_contenedor
# docker rm backforfrontend_contenedor
# docker run -p 8443:8443 --name backforfrontend_contenedor backforfrontend_imagen
# mf1-react-carga
# body1 npm run build