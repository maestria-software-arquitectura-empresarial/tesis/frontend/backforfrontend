const express = require('express');

const router = express.Router();

router.post('/api/statics', (req, res) => {
    console.log(JSON.stringify(req.body));
    // Simulación de datos
    const data = [
        {
            "id_carga": 1,
            "nombre": "Carga BCE 21 04 2023",
            "referencia": "Carga BCE 21 04 2023",
            "descripcion": "Carga BCE 21 04 2023",
            "mail": "prueba@test.com",
            "sms": "09999999999",
            "banco": "Banco Central del Ecuador",
            "fechacarga": "2024-04-21"
        },
        {
            "id_carga": 2,
            "nombre": "Carga BCE 22 04 2023",
            "referencia": "Carga BCE 22 04 2023",
            "descripcion": "Carga BCE 22 04 2023",
            "mail": "prueba@test.com",
            "sms": "09999999999",
            "banco": "Banco Central del Ecuador",
            "fechacarga": "2024-04-22"
        },
        {
            "id_carga": 3,
            "nombre": "Carga BCE 23 04 2023",
            "referencia": "Carga BCE 23 04 2023",
            "descripcion": "Carga BCE 23 04 2023",
            "mail": "prueba@test.com",
            "sms": "09999999999",
            "banco": "Banco Central del Ecuador",
            "fechacarga": "2024-04-23"
        },
        {
            "id_carga": 4,
            "nombre": "Carga BCE 24 04 2023",
            "referencia": "Carga BCE 23 04 2023",
            "descripcion": "Carga BCE 23 04 2023",
            "mail": "prueba@test.com",
            "sms": "09999999999",
            "banco": "Banco Central del Ecuador",
            "fechacarga": "2024-04-23"
        }
    ];

    const fechasRecuento = {};

    const total = data.length;
    data.forEach(item => {
        if (fechasRecuento[item.fechacarga]) {
            fechasRecuento[item.fechacarga]++;
        } else {
            fechasRecuento[item.fechacarga] = 1;
        }
    });
    // Obtener el total de fechas distintas
    // const fechasUnicas = [...new Set(data.map(item => item.fechacarga))];
    // const totalFechas = fechasUnicas.length;

    // Construir la estructura de respuesta
    const respuesta = {
        total,
        total_fechas: fechasRecuento ,
        detalle: data
    };
    console.log(JSON.stringify(respuesta, null, 2));

    // Envía la respuesta como un objeto JSON con los datos simulados
    res.json(respuesta);

});

module.exports = router;
