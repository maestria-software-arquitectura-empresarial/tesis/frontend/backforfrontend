// const express = require('express');
// const https = require('https');
// const fs = require('fs');

// const app = express();

// app.use(express.json());

// app.use((req, res, next) => {
//     const allowedOrigins = ['http://localhost:3000', 'http://localhost:9000', 'http://localhost:443']; 
//     const origin = req.headers.origin;
//     if (allowedOrigins.includes(origin)) {
//         res.setHeader('Access-Control-Allow-Origin', origin);
//     }
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     next();
// });

// const options = {
//     hostname: '186.69.241.179',
//     port: 63579,
//     path: '/inputFrames',
//     method: 'POST',
//     rejectUnauthorized: false 
// };

// app.post('/api/data', (req, res) => {
//     const requestBody = JSON.stringify(req.body);

//     const proxyReq = https.request({
//         ...options,
//         headers: {
//             'Content-Type': 'application/json',
//             'Content-Length': Buffer.byteLength(requestBody)
//         }
//     }, (proxyRes) => {
//         proxyRes.pipe(res, { end: true });
//     });

//     proxyReq.on('error', (err) => {
//         console.error('Error en la solicitud proxy:', err);
//         res.status(500).send('Error en la solicitud proxy');
//     });

//     proxyReq.write(requestBody);
//     proxyReq.end();
// });

// app.post('/api/statics', (req, res) => {
//     console.log(JSON.stringify(req.body));
// });


// const PORT = 8443;
// app.listen(PORT, () => {
//     console.log(`Servidor de proxy en ejecución en el puerto ${PORT}`);
// });


const express = require('express');
const bodyParser = require('body-parser');
const postDataRouter = require('./postData');
const staticsDataRouter = require('./staticsData');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
    const allowedOrigins = ['http://localhost:3000', 'http://localhost:9000', 'http://localhost:443']; 
    const origin = req.headers.origin;
    if (allowedOrigins.includes(origin)) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(postDataRouter);
app.use(staticsDataRouter);

const PORT = 8443;
app.listen(PORT, () => {
    console.log(`Servidor de proxy en ejecución en el puerto ${PORT}`);
});
