const express = require('express');
const https = require('https');

const router = express.Router();

const options = {
    hostname: '186.69.241.179',
    port: 63579,
    path: '/inputFrames',
    method: 'POST',
    rejectUnauthorized: false 
};

router.post('/api/data', (req, res) => {
    const requestBody = JSON.stringify(req.body);

    const proxyReq = https.request({
        ...options,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(requestBody)
        }
    }, (proxyRes) => {
        proxyRes.pipe(res, { end: true });
    });

    proxyReq.on('error', (err) => {
        console.error('Error en la solicitud proxy:', err);
        res.status(500).send('Error en la solicitud proxy');
    });

    proxyReq.write(requestBody);
    proxyReq.end();
});

module.exports = router;
